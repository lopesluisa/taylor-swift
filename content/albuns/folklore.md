---
title: "Folklore"
date: 2020-07-24
---
Lançado durante a pandemia, "Folklore" apresenta uma abordagem mais introspectiva e minimalista, afastando-se das produções grandiosas de seus álbuns anteriores. Com uma sonoridade indie-folk, as letras são recheadas de narrativas e personagens fictícios, refletindo um novo nível de maturidade artística. Canções como "Cardigan" e "Exile" mostram sua habilidade em contar histórias complexas e emocionais. O álbum foi aclamado pela crítica e ganhou o Grammy de Álbum do Ano, destacando seu impacto no cenário musical.