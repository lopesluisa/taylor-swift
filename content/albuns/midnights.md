---
title: "Midnights"
date: 2022-10-21
---
"Midnights" é um álbum que explora os pensamentos e reflexões de Taylor durante a madrugada. Com uma sonoridade pop eletrônica, as letras abordam inseguranças, amor e experiências pessoais em um formato mais íntimo. Canções como "Anti-Hero" e "Lavender Haze" mostram seu estilo de escrita sofisticado e a capacidade de capturar emoções complexas. O álbum foi amplamente elogiado e consolidou ainda mais seu status como uma das principais compositoras da música contemporânea.