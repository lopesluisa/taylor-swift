---
title: "Lover"
date: 2019-08-23
---
"Lover" é um álbum mais otimista e romântico, refletindo um retorno ao amor e à felicidade. As faixas exploram a diversidade das relações, desde o amor romântico até a amizade. Com sucessos como "ME!" e "You Need to Calm Down", Taylor traz um som mais leve e alegre, combinando elementos pop e synth. O álbum foi elogiado por suas letras sinceras e pela produção colorida, marcando um novo capítulo na carreira de Taylor, onde ela se sente mais livre para expressar seu verdadeiro eu.