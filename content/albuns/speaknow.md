---
title: "Speak Now"
date: 2010-10-25
---
Este álbum é notável por ser completamente escrito por Taylor, refletindo sua crescente maturidade como compositora. "Speak Now" é uma coleção de histórias sobre amor, arrependimento e autoafirmação, muitas vezes com um toque autobiográfico. As faixas variam de baladas emocionantes, como "Back to December", a canções mais alegres como "Mine". A produção combina elementos de country e pop, e o álbum é amplamente aclamado por sua profundidade lírica e narrativa coesa.