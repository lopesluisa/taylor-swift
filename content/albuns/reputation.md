---
title: "Reputation"
date: 2017-11-10
---
"Reputation" é um álbum que explora os altos e baixos da fama e da vida pessoal de Taylor. Com uma estética mais sombria e influências de EDM e hip-hop, o álbum apresenta letras que falam sobre traição, amor e reinvenção. "Look What You Made Me Do" e "Delicate" ilustram sua jornada emocional e a luta para recuperar sua imagem pública. O álbum foi bem recebido pela crítica e mostrou uma nova faceta da artista, com uma produção ousada e letras provocativas.