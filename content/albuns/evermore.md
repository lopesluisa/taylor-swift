---
title: "Evermore"
date: 2020-12-11
---
Considerado um "irmão" de "Folklore", "Evermore" continua a exploração de narrativas e temas sombrios, mantendo a sonoridade folk. O álbum inclui colaborações com artistas como Bon Iver e Haim, e explora temas de amor, perda e reflexão. Com faixas como "Willow" e "Champagne Problems", Taylor demonstra uma profundidade lírica e um crescimento artístico significativo. "Evermore" foi bem recebido e solidificou a nova direção de sua música.